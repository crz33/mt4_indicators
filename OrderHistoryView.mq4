//+------------------------------------------------------------------+
//|                                                       Viewer.mq4 |
//|                                           Copyright 2019, crz33. |
//|                                            https://www.crz33.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, crz33."
#property link      "https://www.crz33.com"
#property version   "1.00"
#property strict
#property indicator_chart_window

input string   prefix="crz33";

int OnInit() {
   createButton(prefix + "load_btn", "load", 40, 40, 70);
   createButton(prefix + "prev_btn", "<", 120, 40, 20);
   createButton(prefix + "next_btn", ">", 150, 40, 20);
   createText(prefix + "no_text", 40, 80, 100);
   createButton(prefix + "this_btn", "@", 150, 80, 20);
   return(INIT_SUCCEEDED);
}

void OnDeinit(const int reason){
   ObjectDelete(0, prefix + "next_btn");
   ObjectDelete(0, prefix + "prev_btn");
   ObjectDelete(0, prefix + "load_btn");
   ObjectDelete(0, prefix + "no_text");
   ObjectDelete(0, prefix + "this_btn");   
}

int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[]) {
                
   return(rates_total);
   
}

void OnTimer() {
}

void OnChartEvent(const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam) {
}

//http://yukifx.web.fc2.com/sub/reference/02_stdconstans/object/object_z_OBJ_BUTTON.html
void createButton(string button_id, string text, int x, int y, int w){
   //ボタン作成 (チャートID、オブジェクトID、オブジェクトタイプ、サブウィンドウ番号、1番目の時間のアンカーポイント、1番目の価格のアンカーポイント)
   ObjectCreate(0, button_id, OBJ_BUTTON, 0, 0, 0);
   ObjectSetInteger(0, button_id, OBJPROP_COLOR, clrWhite);//色設定
   ObjectSetInteger(0, button_id, OBJPROP_BACK, false);//オブジェクトの背景表示設定
   ObjectSetInteger(0, button_id, OBJPROP_SELECTABLE, false);// オブジェクトの選択可否設定
   ObjectSetInteger(0, button_id, OBJPROP_SELECTED, false);// オブジェクトの選択状態
   ObjectSetInteger(0, button_id, OBJPROP_HIDDEN, true);// オブジェクトリスト表示設定
   ObjectSetInteger(0, button_id, OBJPROP_ZORDER, 0);// オブジェクトのチャートクリックイベント優先順位
   
   ObjectSetString(0, button_id, OBJPROP_TEXT, text);// 表示するテキスト
   ObjectSetString(0, button_id, OBJPROP_FONT, "ＭＳ　ゴシック");// フォント
   
   ObjectSetInteger(0, button_id, OBJPROP_FONTSIZE, 12);// フォントサイズ
   ObjectSetInteger(0, button_id, OBJPROP_CORNER, CORNER_LEFT_UPPER);// コーナーアンカー設定
   ObjectSetInteger(0, button_id, OBJPROP_XDISTANCE, x);// X座標
   ObjectSetInteger(0, button_id, OBJPROP_YDISTANCE, y);// Y座標
   ObjectSetInteger(0, button_id, OBJPROP_XSIZE, w);// ボタンサイズ幅
   ObjectSetInteger(0, button_id, OBJPROP_YSIZE, 30);// ボタンサイズ高さ
   ObjectSetInteger(0, button_id, OBJPROP_BGCOLOR, clrNavy);// ボタン色
   ObjectSetInteger(0, button_id, OBJPROP_BORDER_COLOR, clrWhite);// ボタン枠色
   ObjectSetInteger(0, button_id, OBJPROP_STATE, false);// ボタン押下状態
   
}

//http://yukifx.web.fc2.com/sub/reference/02_stdconstans/object/object_z_OBJ_EDIT.html
void createText(string text_id, int x, int y, int w){
   //ボタン作成 (チャートID、オブジェクトID、オブジェクトタイプ、サブウィンドウ番号、1番目の時間のアンカーポイント、1番目の価格のアンカーポイント)
   ObjectCreate(0, text_id, OBJ_EDIT, 0, 0, 0);

   ObjectSetInteger(0, text_id, OBJPROP_COLOR, clrWhite);// 色設定
   ObjectSetInteger(0, text_id, OBJPROP_BACK, false);// オブジェクトの背景表示設定
   ObjectSetInteger(0, text_id, OBJPROP_SELECTABLE, false);// オブジェクトの選択可否設定
   ObjectSetInteger(0, text_id, OBJPROP_SELECTED, false);// オブジェクトの選択状態
   ObjectSetInteger(0, text_id, OBJPROP_HIDDEN, true);// オブジェクトリスト表示設定
   ObjectSetInteger(0, text_id, OBJPROP_ZORDER, 0);// オブジェクトのチャートクリックイベント優先順位
   
   
   ObjectSetString(0, text_id, OBJPROP_TEXT, "0");// 表示するテキスト
   ObjectSetString(0, text_id, OBJPROP_FONT, "ＭＳ　ゴシック");// フォント
   
   ObjectSetInteger(0, text_id, OBJPROP_FONTSIZE, 12);// フォントサイズ
   ObjectSetInteger(0, text_id, OBJPROP_CORNER, CORNER_LEFT_UPPER);// コーナーアンカー設定
   ObjectSetInteger(0, text_id, OBJPROP_XDISTANCE, x);// X座標
   ObjectSetInteger(0, text_id, OBJPROP_YDISTANCE, y);// Y座標
   ObjectSetInteger(0, text_id, OBJPROP_XSIZE, w);// ボタンサイズ幅
   ObjectSetInteger(0, text_id, OBJPROP_YSIZE, 30);// ボタンサイズ高さ
   ObjectSetInteger(0, text_id, OBJPROP_BGCOLOR, clrNavy);// ボタン色
   ObjectSetInteger(0, text_id, OBJPROP_BORDER_COLOR, clrWhite);// ボタン枠色
   ObjectSetInteger(0, text_id, OBJPROP_ALIGN, ALIGN_RIGHT);// テキスト整列
   ObjectSetInteger(0, text_id, OBJPROP_READONLY, false);// 読み取り専用設定   
}