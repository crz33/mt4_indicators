//+------------------------------------------------------------------+
//|                                                       Export.mq4 |
//|                                           Copyright 2019, crz33. |
//|                                            https://www.crz33.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, crz33."
#property link      "https://www.crz33.com"
#property version   "1.00"
#property strict
#property indicator_chart_window

//プロパティ
input string startTime = "2000.1.1 00:00";   //抽出開始日時
input string endTime = "3000.1.1 00:00";     //抽出終了日時

int OnInit() {

   //ログファイル名を作る
   //  通貨ペア+時間軸+実行日時(EURUDS_60_20180319215100.csv)
   datetime localtime = TimeLocal();
   string filename = Symbol() + "_" + IntegerToString(Period()) + "_";
   filename += IntegerToString(TimeYear(localtime)) + IntegerToString(TimeMonth(localtime), 2, '0') + IntegerToString(TimeDay(localtime), 2, 0);
   filename += IntegerToString(TimeHour(localtime), 2, '0') + IntegerToString(TimeMinute(localtime), 2, '0') + IntegerToString(TimeSeconds(localtime), 2, '0');
   filename += ".csv";

   //抽出期間を文字列からdatetimeへ変換
   datetime from = StringToTime(startTime);
   datetime to = StringToTime(endTime);

   //書き込み用のCSVファイルオープン
   int handle = FileOpen(filename, FILE_WRITE | FILE_CSV, ",");
   
   //ヘッダ
   FileWrite(handle, "Time", "Open", "High", "Low", "Close");
   
   //チャートのサイズ
   int size = ArraySize(Time);
   
   //過去から出力
   for(int i = size - 1; i >= 0; i--){

      //期間の範囲外は無視
      if(Time[i] < from || to <= Time[i]){
         continue;
      }
      
      //出力
      FileWrite(
         handle,
         //IntegerToString(Time[i]),
         Time[i],
         Open[i],
         High[i],
         Low[i],
         Close[i]
      );

   }
   
   //ファイルクローズ
   FileClose(handle);
   
   return(INIT_SUCCEEDED);
}

int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[]) {
   return(rates_total);
}
